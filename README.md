# htcondor-mon
Monitoring and reporting tools for htcondor workflows.

Goals include:
 * Monitor DAGMan progress
 * Identify problematic OSG sites
 * Monitor and debug CVMFS issues

## Webpages
Use:
 * [https://www.code.daniel-williams.co.uk/otter/installation.html](otter) (webpages)
 * [https://graphviz.readthedocs.io/en/stable/manual.html](graphviz) (graph drawing)
 * [https://research.cs.wisc.edu/htcondor/manual/v8.4/condor-V8_4_12-Manual.pdf](condor manual) (see page 119 for visualizing DAGs)

