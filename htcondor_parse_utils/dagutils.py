# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2018-2019
"""Utilities to extract summary information and completion statistics from
DAGMan logs.  Uses the dagman.log file with event codes:

    * 000: Job submitted
    * 001: Executing on host
    * 028: Job ad info updated
"""

import datetime
import re
import collections

YEAR = str(datetime.datetime.now().year-2000)

LogFiles = collections.namedtuple('LogFiles', ['dagman', 'dagman_out',
                                               'dagman_log',
                                               'dagman_nodes_log'])

CondorJob = collections.namedtuple('CondorJob', ['node', 'job_id',
                                                 'cluster_id',
                                                 'submit_time',
                                                 'terminate_time',
                                                 'job_dict'])

class Dag(object):
    """
    Summary statistics for DAGMan workflowm, parsed from metrics files and
    condor logs
    """
    def __init__(self, dagfile):

        self._get_dagman_logs(dagfile)

        # DAGMan performance
        self._parse_dag_metrics()

        ## Jobs data
        self._parse_job_attrs()

    def _get_dagman_logs(self, dagfile):
        """
        Enumerate dagman log files
        """
        self.log_files = LogFiles(dagfile,
                                  dagfile + '.dagman.out',
                                  dagfile.replace('.dag', '.log'),
                                  dagfile + '.nodes.log')
        return


    def _parse_dag_metrics(self):
        """
        Get a summary of the current workflow.  Returns a dictionary for each of the attributes:
            * timing
            * status
            * configuration.
        Similar to the metrics data but exists from the beginning of the
        workflow.

        """
        #XXX: could be a function

        # Read dagman_out data:
        with open(self.log_files.dagman_out, 'r') as dof:
            dagman_out = dof.readlines()

        # Strip time stamps
        messages = [line.split(' ', 2)[-1] for line in dagman_out]

        ##
        ## Timing
        ##
        self.time_metrics = dict.fromkeys(['start_time', 'end_time', 'duration'])

        # Fix stupidly formatted date
        sensible_date  = lambda date: "/".join([date.split('/')[1],date.split('/')[0],date.split('/')[2]])

        date, time = dagman_out[0].split(' ', 2)[:2]
        self.time_metrics["start_time"] = dagman_time(sensible_date(date), time)

        date, time = dagman_out[-1].split(' ', 2)[:2]
        self.time_metrics["end_time"] = dagman_time(sensible_date(date), time)

        self.time_metrics["duration"] = (self.time_metrics["end_time"] -
                                         self.time_metrics["start_time"]).total_seconds()
        hours, seconds =  self.time_metrics["duration"] // 3600, \
            self.time_metrics["duration"] % 3600
        minutes, seconds = seconds // 60, seconds % 60
        self.time_metrics["human_duration"] = "%dh:%dm:%ds" % (hours, minutes, seconds)

        ##
        ## DAG run status (Done, Failed etc)
        ##
        self.status = dict.fromkeys(['Pre', 'Post', 'Failed', 'Done', 'Queued', 'Total'])
        m_i = [idx for idx, m in enumerate(messages) if 'Done' in m][-1]
        keys = [m for m in messages[m_i].split()]
        vals = [int(m) for m in messages[m_i+2].split()]
        for k, key in enumerate(keys):
            self.status[key] = vals[k]

        # Total jobs
        reg = re.compile(r'Dag contains .*\d total jobs')
        total_jobs_str = filter(reg.match, messages)[0]
        self.status["Total"] = int("".join(re.findall(r'\d', total_jobs_str)))

        ##
        ## Condor configuration
        ##
        self.configuration = dict.fromkeys(['dagman_id'])
        # Find all digit strings longer than 2 in the 2nd line of the file with
        # no upper bound on how long the string could be
        reg = re.compile('.* condor_scheduniv_exec.*STARTING UP\n')
        self.configuration["dagman_id"] = re.findall(r'[0-9]{2,}',
                filter(reg.match, messages)[0])[0]

        # Note: the pattern here is
        #   1. filter the list of messages through a regex to find the lines we want
        #   2. pull out the part we want from the first line using findall

        # Job summaries

        return

    def _parse_job_attrs(self):
        """
        Collection of job objects

        Gets lists of node names and cluster IDs then iterates through to find
        each job's attributes.
        """

        ## Read dagman logs:
        with open(self.log_files.dagman_log, 'r') as logdata:
            dagman_log = logdata.readlines()

        ## Read dagman nodes log:
        with open(self.log_files.dagman_nodes_log, 'r') as nodesdata:
            dagman_nodes = nodesdata.readlines()

        nodes, job_ids, cluster_ids = _get_node_ids(dagman_nodes)

        self.jobs = list()
        job_ids = job_ids
        for j, job_id in enumerate(job_ids):

            print "Analyzing job {0} ({1}/{2})".format(job_id, j+1, len(job_ids))


            self.jobs.append(CondorJob(nodes[j], job_ids[j], cluster_ids[j],
                                       _get_submit_time(dagman_log, job_id),
                                       _get_terminate_time(dagman_log, job_id),
                                       _parse_job_ad_info(dagman_log, job_id)))

        return

def _parse_job_ad_info(dagman_log, job_id):
    """
    Parse all 028 events (execution) for this job ID

    Finds all 028 lines then parses up to 100 lines after that until it hits a
    line beginning with "...".  Lines are parsed into a dictionary of job
    classads. E.g., a line is converted into a dictionary entry like:

        JOB_GLIDEIN_SiteWMS_Queue = "ldas-grid.ligo.caltech.edu"

    becomes:

        job_dict[JOB_GLIDEIN_SiteWMS_Queue] = "ldas-grid.ligo.caltech.edu"
    """

    ## Find most recent execution event for this job
    # XXX: Is the most recent data sufficient?  This will not allow us to
    # track a job's history

    reg = re.compile(r'028 \({}\).*'.format(job_id))
    exec_event = filter(reg.match, dagman_log)[-1] # -1 -> most recent

    job_dict = dict()
    idx = dagman_log.index(exec_event)
    for ei in xrange(1, 100):
        # Make this a larger number than the number of lines associated with
        # this event
        if re.findall(r'\.\.\.', dagman_log[idx+ei]):
            break
        # otherwise evaluate the condor ad expression and strip out newlines
        # and whitespace
        var, val = dagman_log[idx+ei].rstrip().split("=", 1)
        job_dict[var.strip()] = str(val.strip())

    return job_dict

def _get_submit_time(dagman_log, job_id):

    # --- Submit Time
    reg = re.compile(r'.*\(%s\).*Job submitted from host.*\n' % job_id)
    submit_line = re.findall(r'.*Job.*', filter(reg.match, dagman_log)[0])[0]
    date = YEAR + '/' + submit_line.split(' ', 4)[-3]
    time = submit_line.split(' ', 4)[-2]

    return  dagman_time(date, time, True)

def _get_terminate_time(dagman_log, job_id):

    # --- Submit Time
    reg = re.compile(r'.*\(%s\).*Job terminated\.\n' % job_id)
    #term_line = re.findall(r'.*Job.*', filter(reg.match, dagman_log)[0])[0]
    try:
        term_line = filter(reg.match, dagman_log)[0]
        date = YEAR + '/' + term_line.split(' ', 4)[-3]
        time = term_line.split(' ', 4)[-2]
        return  dagman_time(date, time, True)
    except:
        return 0



def _get_node_ids(dagman_nodes):

    ## Get node names
    nodes = [re.findall(r'.*DAG Node: (\w+)', line)[0] for line in dagman_nodes if
             re.findall(r'.*DAG Node: (\w+)', line)]

    ## Get Job IDs
    reg = re.compile(r'000 \(.*\).*\n')
    job_ids = [re.findall(r'(?<=\()(.*?)(?=\))', line)[0] for line in
               filter(reg.match, dagman_nodes) if
               re.findall(r'(?<=\()(.*?)(?=\))', line)[0]]
    # Note: the pattern here is:
    #   1. filter the list of messages through a regex to find the lines we want
    #   2. pull out the part we want from ALL lines using findall IFF there is a match
    cluster_ids = [cid.split('.')[0] for cid in job_ids]

    return nodes, job_ids, cluster_ids


def dagman_time(datestr, timestr, year_first=False):
    """
    Create datetime object from a line in a dagman.out file
    """
    if year_first:
        date = list(map(int, datestr.split('/')))
    else:
        date = list(map(int, datestr.split('/')[::-1]))
    date[0] += 2000
    time = list(map(int, timestr.split(':')))

    return datetime.datetime(*(date+time))
