#!/usr/bin/env python
# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2018-2019
"""Lightweight script to run tests and components for BayesWave analysis with
informative error codes.
"""
import os
import sys
import argparse
import collections
import subprocess
from random import randrange, random
import time

FrameRead = collections.namedtuple('FrameRead', ['ifo', 'frame_type'])

def run_times(obs_run):
    """
    Draw a random time from the observing epoch in obs_run.

    See e.g., https://wiki.ligo.org/LSC/JRPComm/EngineeringRuns
    """

    obs_runs = {'ER8':(1123858817, 1126623617),
                'O1':(1126051217, 1137254417),
                'ER9':(1152136817, 1152169157),
                'O2':(1164499217, 1187654418)}

    try:
        return obs_runs[obs_run]
    except KeyError:
        print "Observing run must be one of ", obs_runs.keys()

def exec_datafind(frame, gps_start, gps_end):
    """
    String to execute datafind command
    """

    cache_file = frame.ifo+'.lcf'

    try:
        subprocess.check_output(["/usr/bin/gw_data_find",
                                 "--observatory", frame.ifo[0],
                                 "--type", frame.frame_type,
                                 "--gps-start-time", str(gps_start),
                                 "--gps-end-time", str(gps_end),
                                 "--server", "datafind.ligo.org",
                                 "--url-type", "file",
                                 "--lal-cache",
                                 "--output-file", cache_file],
                                stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as datafind_fail:
        print datafind_fail.output
        sys.exit(datafind_fail.returncode)

    return cache_file

def draw_cache_time(cache_filename, datalen=4):
    """
    Draw a random trigger time to analyze from the frames listed in cache_file
    """
    # Read frame cache and select a random time
    with open(cache_filename) as lcf:
        cache = lcf.read().splitlines()
    lcf.close()
    frame_starts = [float(line.split()[2]) for line in cache]
    frame_lengths = [float(line.split()[3]) for line in cache]

    if not frame_starts:
        print "Frame cache empty!"
        sys.exit(-1)

    # Select a time from a random frame (including a buffer of datalen)
    idx = randrange(len(frame_starts))

    # Check that selected frame has acceptable length of data
    while frame_lengths[idx] < datalen:
        idx = randrange(len(frame_starts))

    start_time = frame_starts[idx] + 0.5*datalen
    end_time = frame_starts[idx] + frame_lengths[idx] - 0.5*datalen

    return start_time + (end_time-start_time)*random()


def parse_cmdline():
    """Parse command line"""

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("--obs-run", type=str, default=None,
                        help="""Observing epoch (e.g., "O2") """)
    parser.add_argument("--gps-start", type=int, default=815097613, # S5
                        help="""GPS start time for triggers""")
    parser.add_argument("--gps-end", type=int, default=int(time.time())-315964782,
                        help="""GPS end time for triggers""")
    parser.add_argument("--frame-type", type=str, default="H1_HOFT_C02",
                        help="""Type of frame to read""")
    parser.add_argument("--num-times", type=int, default=100,
                        help="""Number of trigger times to sample""")
    parser.add_argument("--out-file", type=str, default=None,
                        help="""Override output file name""")

    cmdline_args = parser.parse_args()

    return cmdline_args

#########################################################################

def main():
    """
    Generate a list of job times
    """

    ## Parse input
    args = parse_cmdline()

    ## Output file name
    if args.out_file is None:
        args.out_file = 'trigtimes' + '-' + args.frame_type + '-' + \
                        str(args.gps_start) + '-' + str(args.gps_end - args.gps_start) \
                        + '.txt'
    ifo = args.frame_type.split('_')[0]
    frame_read = FrameRead(ifo, args.frame_type)

    ## Select analysis times
    if args.obs_run is None:
        # Use gps-start/end times
        start_time, end_time = args.gps_start, args.gps_end
    else:
        # Get start / end for these frame types in this run
        start_time, end_time = run_times(args.obs_run)


    ## Create frame file caches
    print "Running datafind"
    cache_file = exec_datafind(frame_read, start_time, end_time)

    with open(args.out_file, 'w') as outfile:
        for _ in xrange(args.num_times):
            outfile.writelines("%f\n" % draw_cache_time(cache_file))


if __name__ == "__main__":

    print "executing"
    main()
    print "complete"
