#!/bin/sh -e

if [ ! -d "bayeswave_output" ]
then
    mkdir bayeswave_output
else
    echo "Warning: bayeswave_output directory exists, removing contents"
    rm -rf bayeswave_output/*
fi

BayesWave \
    --glitchOnly \
    --psdlength 4.0 --noClean  \
    --outputDir bayeswave_output \
    --NCmax 4 --srate 128.0 --seglen 4.0 \
    --checkpoint  \
    --trigtime 1185775528.714499950 \
    --psdstart 1185775526.714499950 --H1-cache datafind/H1.lcf \
    --H1-flow 32.0 --Niter 1000 \
    --NCmin 2 \
    --H1-channel H1:DCS-CALIB_STRAIN_C02 --ifo H1

echo $?

