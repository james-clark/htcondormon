#!/usr/bin/env python
# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2018-2019
"""Lightweight script to run tests and components for BayesWave analysis with
informative error codes.
"""
import os
import sys
import shutil
import argparse
import collections
import subprocess
from random import randrange, random

FrameRead = collections.namedtuple('FrameRead', ['ifo', 'frame_type', 'channel'])

def run_times(obs_run):
    """
    Draw a random time from the observing epoch in obs_run.

    See e.g., https://wiki.ligo.org/LSC/JRPComm/EngineeringRuns
    """

    obs_runs = {'ER8':(1123858817, 1126623617),
                'O1':(1126623617, 1134057617),
                'ER9':(1152136817, 1152169157),
                'O2':(1164499217, 1187654418)}

    try:
        return obs_runs[obs_run]
    except KeyError:
        print "Observing run must be one of ", obs_runs.keys()


def dirsetup(workdir):
    """
    Set up output directories
    """

    ## Create datafind directory
    datafind = os.path.join(workdir, 'datafind')
    if os.path.exists(datafind):
        print "datafind directory exists, removing contents"
        shutil.rmtree(datafind)
    os.makedirs(datafind)

    ## Create BayesWave output directory
    bayeswave_out = os.path.join(workdir, 'bayeswave_output')
    if os.path.exists(bayeswave_out):
        print "bayeswave_out directory exists, removing contents"
        shutil.rmtree(bayeswave_out)
    os.makedirs(bayeswave_out)

    return datafind, bayeswave_out

def exec_datafind(frame, gps_start, gps_end):
    """
    String to execute datafind command
    """

    cache_file = os.path.join('datafind', frame.ifo+'.lcf')

    try:
        datafind_result = subprocess.check_output(["/usr/bin/gw_data_find",
                                                   "--observatory", frame.ifo[0],
                                                   "--type", frame.frame_type,
                                                   "--gps-start-time", str(gps_start),
                                                   "--gps-end-time", str(gps_end),
                                                   "--server", "datafind.ligo.org:443",
                                                   "--url-type", "file",
                                                   "--lal-cache",
                                                   "--output-file", cache_file],
                                                   stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as datafind_fail:
        print datafind_fail.output
        sys.exit(datafind_fail.returncode)

    return cache_file

def exec_bayeswave(frame, trigtime, seglen=4):
    """
    BayesWave execution string
    """

    segment_start = trigtime - 0.5*seglen
    psd_start = trigtime - 0.5*seglen

    try:
        bayeswave_result = subprocess.check_output(["BayesWave",
                                                    "--glitchOnly", "--noClean",
                                                    "--checkpoint",
                                                    "--ifo", frame.ifo,
                                                    "--%s-flow"%frame.ifo, str(32),
                                                    "--%s-channel"%frame.ifo, frame.channel,
                                                    "--%s-cache"%frame.ifo,
                                                    "datafind/%s.lcf"%frame.ifo,
                                                    "--segment-start", str(segment_start),
                                                    "--trigtime", str(trigtime),
                                                    "--psdstart", str(psd_start),
                                                    "--psdlength", str(seglen),
                                                    "--seglen", str(seglen),
                                                    "--NCmin", str(2), "--NCmax", str(2),
                                                    "--srate", str(128),
                                                    "--outputDir", "bayeswave_output",
                                                    "--Niter", str(1000)],
                                                    stderr=subprocess.STDOUT)
        print bayeswave_result

    except subprocess.CalledProcessError as bayeswave_fail:
        print bayeswave_result
        print "BayesWave failure"
        print bayeswave_fail.output
        sys.exit(bayeswave_fail.returncode)

    return

def draw_cache_time(cache_filename, datalen=4):
    """
    Draw a random trigger time to analyze from the frames listed in cache_file
    """
    # Read frame cache and select a random time
    with open(cache_filename) as lcf:
        cache = lcf.read().splitlines()
    lcf.close()
    frame_starts = [float(line.split()[2]) for line in cache]
    frame_lengths = [float(line.split()[3]) for line in cache]

    # Select a time from a random frame (including a buffer of datalen)
    idx = randrange(len(frame_starts))

    # Check that selected frame has acceptable length of data
    while frame_lengths[idx] < datalen:
        idx = randrange(len(frame_starts))

    start_time = frame_starts[idx] + 0.5*datalen
    end_time = frame_starts[idx] + frame_lengths[idx] - 0.5*datalen

    return start_time + (end_time-start_time)*random()


def parse_cmdline():
    """Parse command line"""

    parser = argparse.ArgumentParser(description=__doc__,
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("--workdir", type=str, default="./",
                        help="""Directory to work in""")
    parser.add_argument("--ifo", type=str, default="H1",
                        help="""Instrument to analyze""")
    parser.add_argument("--obs-run", type=str, default="O2",
                        help="""Observing epoch "O2" """)
    parser.add_argument("--frame-type", type=str, default="H1_HOFT_C02",
                        help="""Type of frame to read""")
    parser.add_argument("--channel", type=str,
                        default="H1:DCS-CALIB_STRAIN_C02",
                        help="""frame channel to read""")
    parser.add_argument("--trigtime", type=float, default=None,
                        help="""Specific GPS time to analyze.  Default behavior
                        is to draw a random time in the observing run""")
    parser.add_argument("--clean-up", action='store_true', default=False,
                        help="""Remove datafind and bayeswave dirs""")

    cmdline_args = parser.parse_args()

    return cmdline_args


#########################################################################

def main():
    """
    Run BayesWave on data
    """

    ## Parse input
    args = parse_cmdline()

    frame_read = FrameRead(args.ifo, args.frame_type, args.channel)

    ## Create I/O directory structure
    try:
        datafinddir, bayeswavedir = dirsetup(args.workdir)
    except OSError:
        print "Could not create directory structure"
        sys.exit(-1)

    ## Select science run times
    if args.trigtime is None:
        # Just get start/end times of the entire run
        start_time, end_time = run_times(args.obs_run)
    else:
        # Get a data segment for this time only
        trigtime = args.trigtime
        start_time = trigtime - 2
        end_time = trigtime + 2

    ## Create frame file caches
    print "Running datafind"
    cache_file = exec_datafind(frame_read, start_time, end_time)

    if args.trigtime is None:
        trigtime = draw_cache_time(cache_file)

    ## Run BayesWave
    print "Running BayesWave"
    exec_bayeswave(frame_read, trigtime, seglen=4)

    ## Clean up
    if args.clean_up:
        print "Removing BayesWave I/O"
        shutil.rmtree(datafinddir)
        shutil.rmtree(bayeswavedir)



if __name__ == "__main__":

    print "executing"
    main()
    print "complete"
