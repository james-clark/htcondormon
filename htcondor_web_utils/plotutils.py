# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2018-2019
"""Plotting functions for web reports"""

import math as m
import matplotlib.pyplot as plt


# Pie chart, where the slices will be ordered and plotted counter-clockwise:
def status_pie(status_dict_full):
    """
    Create a pie chart show DAGMan completion status
    """

    ## Reduce dictionary to non-zero values and remove Total jobs
    status_dict = dict(status_dict_full)
    status_dict.pop('Total')
    status_dict = {k:v for k, v in status_dict.items() if v}
    status = status_dict.values()
    labels = [k + ":%.1f%%"%(100*float(v)/float(status_dict_full['Total'])) for k, v
              in status_dict.items()]

    fig1, ax1 = plt.subplots()
    wedges, texts = ax1.pie(status, labels=None, autopct=None,
                            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    ax1.legend(wedges, labels,
               title="DAGMan Status",
               loc="center left"
              )


#       sign = lambda x: (1, -1)[x < 0]
#       bbox_props = dict(boxstyle="square,pad=0.3", fc="w", ec="k", lw=0.72)
#       kw = dict(xycoords='data', textcoords='data',
#                 arrowprops=dict(arrowstyle="-"), bbox=bbox_props, zorder=0,
#                 va="center")
#       for i, p in enumerate(wedges):
#           ang = (p.theta2 - p.theta1)/2. + p.theta1
#           y = m.sin(ang*m.pi/180)
#           x = m.cos(ang*m.pi/180)
#           horizontalalignment = {-1: "right", 1: "left"}[int(sign(x))]
#           connectionstyle = "angle,angleA=0,angleB={}".format(ang)
#           kw["arrowprops"].update({"connectionstyle": connectionstyle})
#           ax1.annotate(labels[i], xy=(x, y), xytext=(1.35*sign(x), 1.4*y),
#                        horizontalalignment=horizontalalignment, **kw)


    return fig1, ax1
