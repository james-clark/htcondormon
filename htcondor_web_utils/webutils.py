#!/usr/bin/env python
# -*- coding:utf-8 -*-
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# Authors:
# - James Alexander Clark, <james.clark@ligo.org>, 2018-2019
"""Web report utilities for htcondormon"""

import os
import otter
import otter.bootstrap as bt
from htcondor_web_utils import plotutils

class WebReport(object):
    """
    Build an otter webpage
    """

    def __init__(self, dag, report_meta, reportdir='.', otter_config=None):

        ## Set report metadata to None so we have something to work with
        if report_meta is None:
            print "No report metadata provided, setting to None"
            report_meta = dict.fromkeys(['analysis_name', 'dagfile', 'author',
                                         'author_email', 'base_url'])
        self.config_file = otter_config

        ## Set up report page
        self.reportdir = os.path.join(reportdir, 'dagreport')
        self.report_meta = report_meta
        try: 
            os.makedirs(reportdir)
        except OSError:
            print "Web report directory exists, overwriting"

        self.web_report = otter.Otter(os.path.join(self.reportdir, "index.html"),
                                      author=report_meta['author'],
                                      title="DAGMan Report: {}".format(
                                          report_meta['analysis_name']),
                                      author_email=report_meta['author_email'],
                                      config_file=self.config_file)

        with self.web_report:
            # First Section
            workflow = os.path.basename(report_meta['dagfile'].replace('.dag', ''))
            self.web_report + "# {}".format(workflow)
            meta = bt.Row(2)
            meta[0] + report_meta
            meta[1] + dag.time_metrics
            meta[1] + dag.configuration
            self.web_report + meta

        return

    def add_dag_metrics(self, dag):
        """
        Add DAG summary stats to the web web_report
        """
        with self.web_report:

            self.web_report + "## DAGMan Status"

            # Status Pie Chart
            pie_fig, pie_ax = plotutils.status_pie(dag.status)

            # Example of a Table
            metrics = bt.Row(2)
            metrics[0] + dag.status
            metrics[1] + pie_fig
            self.web_report + metrics

    def add_jobs(self, dag):

        # Create sub-pages
        with self.web_report:
            self.web_report + "## Job Details"

            self.web_report + " Table shows summary information for all jobs \
                    in this DAG."
            self.web_report + " Information is parsed from the most recent \
                    Job Ad information event (see lines beginning 028) in the \
                    DAGMan log file."
            self.web_report + "Click on <b>ClusterID</b> to see all \
                    recent information about that job."
            self.web_report + "Termination Time is the time from submission \
                    to job termination"
            self.web_report + "Legend: "
            self.web_report + " * <font color='#8C001A'>RED</font>: Exited \
                    with non-zero return value"
            self.web_report + " * <font color='#306754'>GREEN</font>: Exited \
                    normally"
            self.web_report + " * <font color='#C58917'>YELLOW</font>: Job \
                    incomplete"

            self.web_report + "New Features Coming Soon:"
            self.web_report + " * Live / continuous updates"
            self.web_report + " * Proper CLI for general use"
            self.web_report + " * Versioning information on pages"
            self.web_report + " * Site diagnostics plots: where jobs ran etc"
            self.web_report + " * Submission, execution, completion rates as time series"
            self.web_report + " * Direct links to shell command to execute a job"
            self.web_report + " * Direct links to workflow files"
            self.web_report + " * Direct links to job stdout, stderr (required pipeline-specific plugin)"

        self._jobs_summary(dag)



    def _jobs_summary(self, dag):

        jobs_table = dict()
        with self.web_report:
            self.web_report + '<table width=1000px>'
            self.web_report + "<tr align='center', style='background-color:#2C3539;color:#FFFFFF;'>"
            self.web_report + "<td><b>Node</b></td>"
            self.web_report + "<td><b>ClusterID</b></td>"
            self.web_report + "<td><b>ReturnValue</b></td>"
            self.web_report + "<td><b>JOB_Site</b></td>"
            self.web_report + "<td><b>TriggerEventTypeName</b></td>"
            self.web_report + "<td><b>Submission Time</b></td>"
            self.web_report + "<td><b>Time To Termination</b></td>"
            self.web_report + "</tr>"
            for j, job in enumerate(dag.jobs):
                self._add_job_row(job)
            self.web_report + "</table>"


    def _add_job_row(self, job):
        """
        Job rows:
            | DagNode | ClusterID | JOB_Site | Last Event | Time To Completion |
        """

        ## Create the summary page for this job
        job_page = os.path.join(self.reportdir, "%s.html" % job.job_id)
        self._make_job_page(job_page, job)

        ## Set cell color
        try:
            if job.job_dict['ReturnValue'] != 0:
                # Paint it red
                self.web_report + "<tr align='center', style='background-color:#306754;color:#FFFFFF;'>"
            else:
                # Job exited normally
                self.web_report + "<tr align='center', style='background-color:#8C001A;color:#FFFFFF;'>"
        except KeyError:
                self.web_report + "<tr align='center', style='background-color:#C58917;color:#FFFFFF;'>"

        ## Node
        self.web_report + "<td>" + str(job.node) + "</td>"

        ## Cluster ID (takes user to jobAd summary)
        # XXX: needs to be a relative link
        job_page_url = os.path.join(self.report_meta['base_url'], 'dagreport',
                '%s.html' % job.job_id)
        cluster_id_link = "<a href='{0}'><font color='#82CAFF'>{1}</font></a>".format(job_page_url, job.cluster_id)

        self.web_report + "<td>" + cluster_id_link + "</td>"

        ## Return Value
        if "ReturnValue" in job.job_dict:
            self.web_report + "<td>" + job.job_dict["ReturnValue"] + "</td>"
        else:
            # Probably incomplete
            self.web_report + "<td>-</td>"


        ## JOB_Site
        try:
            jobsite = job.job_dict["JOB_Site"]
        except KeyError:
            jobsite = "Unknown"
        self.web_report + "<td>" + jobsite.strip('"') + "</td>"

        ## Last Log Event
        lastevent = job.job_dict["TriggerEventTypeName"]
        self.web_report + "<td>" + lastevent.strip('"') + "</td>"

        ## Submit Time
        self.web_report + "<td>" + str(job.submit_time) + "</td>"

        ## Termination Time
        if "ReturnValue" in job.job_dict:
            seconds = (job.terminate_time - job.submit_time).total_seconds() 
            hours, seconds = seconds // 3600, seconds % 3600
            minutes, seconds = seconds // 60, seconds % 60
            duration_str = "%dh:%dm:%ds" % (hours, minutes, seconds)
        else:
            duration_str = "-"
        self.web_report + "<td>" + duration_str + "</td>"

        self.web_report + "</tr>"

        return

    def _make_job_page(self, job_page, job):
        """
        Sub-page for full job table
        """

        ## Set up header info
        job_report = otter.Otter(job_page,
                                 author=self.report_meta['author'],
                                 title="DAGMan Job Report: {}".format(
                                     self.report_meta['analysis_name']),
                                 author_email=self.report_meta['author_email'],
                                 config_file=self.config_file)

        with job_report:
            job_table = bt.Row(2)
            job_table[0] + "## Summary"
            job_table[0] + " * DAG node (job name): %s" % job.node
            job_table[0] + " * HTCondor ID: %s" % job.job_id
            job_table[0] + " * Submitted: %s" % job.submit_time

            if "ReturnValue" in job.job_dict:
                job_table[0] + " * Terminated: %s" % job.terminate_time
                seconds = (job.terminate_time - job.submit_time).total_seconds()
                hours, seconds = seconds // 3600, seconds % 3600
                minutes, seconds = seconds // 60, seconds % 60
                job_table[0] + " * Condor time: %dh:%dm:%ds" % (hours, minutes, seconds)
            job_table[1] + "## JobAds from most recent 028 event:"
            job_table[1] + job.job_dict
            job_report + job_table

        return
